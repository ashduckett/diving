var PlaceController = function(element, model, view) {
    this.model = model;
    this.view = new PlaceView(model, element, this);
};

PlaceController.prototype.getPlaces = function() {
    return this.model;
};

PlaceController.prototype.render = function() {
    this.view.render();
};

PlaceController.prototype.listOrderChanged = function(source, destination) {
    this.model.swapPlaces(source, destination);
};

PlaceController.prototype.newItemAdded = function(data) {
    console.log('new item added called')
    
    
    
    this.model.save(data);
};