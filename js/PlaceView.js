var PlaceView = function(model, element, controller) {
    this.model = model;
    this.element = element;
    this.lastSourcePosition = null;
    this.lastDestPosition = null;
    this.controller = controller;

    // Will need a new button

};

PlaceView.prototype.render = function() {
    var self = this;
    // Start with a clean slate
    this.element.innerHTML = '';

    // A list view needs a container
    var listContainer = document.createElement('ul');
    listContainer.classList.add('placesViewContainer');
    listContainer.id = 'listViewContainer';
    
    function createTextField(textFieldId, labelText) {
        var textFieldContainer = document.createElement('div');
        textFieldContainer.classList.add('fieldRow');
        var textField = document.createElement('input');
        textField.type = 'text';
        textField.id = textFieldId;

        var textFieldLabel = document.createElement('label');
        textFieldLabel.classList.add('fieldLabel');
        textFieldLabel.innerHTML = labelText;

        textFieldContainer.appendChild(textFieldLabel);
        textFieldContainer.appendChild(textField);
        return textFieldContainer;
    }

    function createFileField(fileFieldId, labelText) {
        var fileFieldContainer = document.createElement('div');
        fileFieldContainer.classList.add('fieldRow');
        var fileField = document.createElement('input');
        fileField.type = 'file';
        fileField.id = fileFieldId;
        var fileFieldLabel = document.createElement('label');
        fileFieldLabel.classList.add('fieldLabel');
        fileFieldLabel.innerHTML = labelText;
        fileFieldContainer.appendChild(fileFieldLabel);
        fileFieldContainer.appendChild(fileField);
        return fileFieldContainer;
    }

    function createSelectField(selectFieldId, labelText, values) {
        var selectFieldContainer = document.createElement('div');
        selectFieldContainer.classList.add('fieldRow');
        var selectFieldLabel = document.createElement('label');
        selectFieldLabel.classList.add('fieldLabel');
        selectFieldLabel.innerHTML = labelText;
        var dropdown = createSelectBox(selectFieldId, values);
        selectFieldContainer.appendChild(selectFieldLabel);
        selectFieldContainer.appendChild(dropdown);
        return selectFieldContainer;
    }

    function createTextAreaField(textAreaId, labelText) {
        var textAreaContainer = document.createElement('div');
        textAreaContainer.classList.add('fieldRow');
        var textAreaLabel = document.createElement('label');
        textAreaLabel.classList.add('fieldLabel');
        textAreaLabel.innerHTML = labelText;
        
        var textAreaControl = document.createElement('textarea'); 
        textAreaControl.id = textAreaId;

        textAreaContainer.appendChild(textAreaLabel);
        textAreaContainer.appendChild(textAreaControl);

        return textAreaContainer;
    }

    function createCheckboxField(overallLabel, labels, ids) {
       var mainLabel = document.createElement('label');
       mainLabel.classList.add('fieldLabel');
       mainLabel.innerHTML = overallLabel;

        var checkboxFieldContainer = document.createElement('div');
        checkboxFieldContainer.classList.add('fieldRow');

        checkboxFieldContainer.appendChild(mainLabel);

        var checkboxesContainer = document.createElement('div');
        checkboxesContainer.classList.add('checkboxesContainer');

        for (var i = 0; i < labels.length; i++) {
            var checkBoxAndLabel = document.createElement('div');
            checkBoxAndLabel.classList.add('checkboxAndLabel');

            var label = document.createElement('label');
            label.classList.add('checkboxLabel');
            label.innerHTML = labels[i];

            var checkboxField = document.createElement('input');
            checkboxField.type = 'checkbox';
            checkboxField.id = ids[i];
            checkBoxAndLabel.appendChild(checkboxField);
            checkBoxAndLabel.appendChild(label);
            checkboxesContainer.appendChild(checkBoxAndLabel);
        }
        checkboxFieldContainer.appendChild(checkboxesContainer)
        return checkboxFieldContainer;
    }



    
    $('a#newButton').click(function() {
        console.log('new place')

        // Clear away the list
        // Display form
        self.element.innerHTML = '';
        self.element.style.width = '100%';

       // I want a form that contains, to start with, all of the common entries needed for both places to go and dive sites
       // var isDiveSite = createSelectBox('isDiveSiteDropdown', ['Dive Site', 'Place to Go']);
        
        var isDiveSite = createSelectField('diveSite', 'Dive Site/Place to Go', ['Dive Site', 'Place to Go']);
        var heading = createTextField('previewHeadingField', 'Heading');
        var subheading = createTextField('previewSubheadingField', 'Subheading')
        var thumbnail = createFileField('previewThumbnailFile', 'Thumbnail');
        var rating = createSelectField('rating', 'Rating', ['0', '0.5', '1', '1.5', '2', '2.5', '3', '3.5', '4', '4.5', '5']);
        var ratingCommentField = createTextAreaField('ratingComment', 'Rating Comment');
        var islandCheckboxField = createCheckboxField('Island(s)', ['Koh Tao', 'Koh Samui', 'Koh Chang'], ['checkboxKohTao', 'checkboxKohSamui', 'checkboxKohChang']);
        var virtualTourField = createTextField('previewVirtualTourField', 'Virtual Tour');




// Historical, Religious, Culture, Nature, Natural Wonders, Wildlife 
        var regionCheckboxField = createCheckboxField('Region(s)', ['Central', 'Eastern', 'Northern', 'North Eastern', 'Southern', 'Western'], ['checkboxCentral', 'checkboxEastern', 'checkboxNorthern', 'checkboxNorthEastern', 'checkboxSouthern', 'checkboxWestern']);
        var placeTypesField = createCheckboxField('Place Type(s)', ['Historical', 'Religious', 'Culture', 'Nature', 'Natural Wonders', 'Wildlife'], ['checkboxHistorical', 'checkboxReligious', 'checkboxCulture', 'checkboxNature', 'checkboxNaturalWonders', 'checkboxWildlife']);
        var locationTextControl = createTextField('locationTextTile', 'Location Text Title');
        var locationTextField = createTextAreaField('locationText', 'Location Text');
        var depthField = createTextField('previewDepth', 'Depth');
        var visibilityField = createTextField('previewVisibility', 'Visibility');
        var diverLevelField = createSelectField('diverLevelDropdown', 'Diver Level', ['1', '2', '3', '4', '5']);
        var diverCurrentField = createSelectField('diverCurrentDropdown', 'Diver Level', ['Calm', 'Weak', 'Average', 'Strong', 'Rough']);
        var locationImageOneField = createFileField('locationImageOne', 'Primary Location Image'); 
        var locationImageTwoField = createFileField('locationImageTwo', 'Secondary Location Image'); 
        var locationImageThreeField = createFileField('locationImageThree', 'Secondary Location Image');
        var locationImageFourField = createFileField('locationImageFour', 'Secondary Location Image');
        var isPaidField = createSelectField('isPaidSelect', 'Admission', ['Paid', 'Free Entry']);





        var submitButton = document.createElement('a')
        submitButton.href = '#';
        submitButton.innerHTML = 'Generate Preview';

        var saveButton = document.createElement('a')
        saveButton.href = '#';
        saveButton.innerHTML = 'Save';






        var form = document.createElement('form');
        form.classList.add('adminForm');

        form.appendChild(isDiveSite);
        form.appendChild(heading)
        form.appendChild(subheading)
        form.appendChild(thumbnail)
        form.appendChild(rating)
        form.appendChild(ratingCommentField)
        //form.appendChild(islandChoicesContainer)
        form.appendChild(islandCheckboxField)
        form.appendChild(locationTextControl)
        form.appendChild(locationTextField)
        form.appendChild(virtualTourField);
        form.appendChild(depthField)
        form.appendChild(visibilityField);
        form.appendChild(diverLevelField)
        form.appendChild(diverCurrentField)
        form.appendChild(locationImageOneField)
        form.appendChild(locationImageTwoField)
        form.appendChild(locationImageThreeField)
        form.appendChild(locationImageFourField)
        //form.appendChild(regionChoicesContainer);#
        form.appendChild(regionCheckboxField);
        form.appendChild(placeTypesField);
        form.appendChild(isPaidField)

        form.appendChild(submitButton)
        form.appendChild(saveButton)

        self.element.appendChild(form);
        $('.adminDropdown').selectmenu();



        // We will need a preview thumbnail
        var thumbnailPreviewContainer = document.createElement('div');
        thumbnailPreviewContainer.classList.add('diveSites');
        thumbnailPreviewContainer.style.backgroundColor = 'red';

        var thumbnailPreview = document.createElement('div');
        thumbnailPreview.classList.add('site');

        // Now the contents of the site element
        var paddingTrick = document.createElement('div');
        paddingTrick.classList.add('paddingTrick');

        var siteImage = document.createElement('img');
        siteImage.classList.add('islandImage');
        siteImage.src = 'img/divesites/thumbnails/ban_kwan_chang.jpg';

        var darkeningOverlay = document.createElement('div');
        darkeningOverlay.classList.add('darkeningOverlay');
        
        thumbnailPreview.appendChild(paddingTrick);
        thumbnailPreview.appendChild(siteImage);
        thumbnailPreview.appendChild(darkeningOverlay);


        var siteTextOverlay = document.createElement('div');
        siteTextOverlay.classList.add('siteTextOverlay');

        var header = document.createElement('h1');
        header.classList.add('largeHeader');
        header.innerHTML = 'header placeholder';
        siteTextOverlay.appendChild(header);

        var thumbnailPreviewText = document.createElement('p');
        thumbnailPreviewText.classList.add('standardSectionText');
        thumbnailPreviewText.innerHTML = 'text placeholder';
        siteTextOverlay.appendChild(thumbnailPreviewText);

        var thumbnailPreviewButton = document.createElement('a');
        thumbnailPreviewButton.href = '#';
        thumbnailPreviewButton.classList.add('btn');
        thumbnailPreviewButton.classList.add('btn-dark-blue');
        thumbnailPreviewButton.innerHTML = 'button';
        siteTextOverlay.appendChild(thumbnailPreviewButton);

        thumbnailPreview.appendChild(siteTextOverlay);

        thumbnailPreviewContainer.appendChild(thumbnailPreview);
        self.element.appendChild(thumbnailPreviewContainer);

        var placePreview = document.createElement('div');
        placePreview.classList.add('placePreview');
        self.element.appendChild(placePreview);

        // Get hold of content. Let's say, for example, sail rock, which has an id of 1.



        // For preview purposes we need a url to be able to show the thumbnail
        var thumbNailSrc = null;
        var thumbnailControl = $('#previewThumbnailFile');

        var locationImageOneSrc = null;
        var locationImageOneControl = $('#locationImageOne');

        var locationImageTwoSrc = null;
        var locationImageTwoControl = $('#locationImageTwo');

        var locationImageThreeSrc = null;
        var locationImageThreeControl = $('#locationImageThree');

        var locationImageFourSrc = null;
        var locationImageFourControl = $('#locationImageFour');

        function preview_image(event) {
            var reader = new FileReader();
            
            reader.onload = function() {
                switch(event.target.id) {
                    case 'previewThumbnailFile':
                        thumbNailSrc = reader.result;
                        break;
                    case 'locationImageOne':
                        locationImageOneSrc = reader.result;
                        break;
                    case 'locationImageTwo':
                        locationImageTwoSrc = reader.result;
                        break;
                    case 'locationImageThree':
                        locationImageThreeSrc = reader.result;
                        break;
                    case 'locationImageFour':
                        locationImageFourSrc = reader.result;
                        break;
                }
                

            }
            reader.readAsDataURL(event.target.files[0]);
        }
        
        $(thumbnailControl).on('change', function(event) {
            preview_image(event);
        });

        $(locationImageOneControl).on('change', function(event) {
            preview_image(event);
            
        });

        $(locationImageTwoControl).on('change', function(event) {
            preview_image(event);
        });

        $(locationImageThreeControl).on('change', function(event) {
            preview_image(event);
        });

        $(locationImageFourControl).on('change', function(event) {
            preview_image(event);
        });


        function getFilePathExtension(path) {
            var filename = path.split('\\').pop().split('/').pop();
            var lastIndex = filename.lastIndexOf(".");
            if (lastIndex < 1) return "";
            return filename.substr(lastIndex + 1);
        }

        function save(id = null) {
            
//            formData.append('locationImagePaths', locationImagePaths);

            var pathOne = $('#previewHeadingField').val().replace(/\s+/g, '_').toLowerCase() + '_1.' + getFilePathExtension($('#locationImageOne').val());
            var pathTwo = $('#previewHeadingField').val().replace(/\s+/g, '_').toLowerCase() + '_2.' + getFilePathExtension($('#locationImageTwo').val());
            var pathThree = $('#previewHeadingField').val().replace(/\s+/g, '_').toLowerCase() + '_3.' + getFilePathExtension($('#locationImageThree').val());
            var pathFour = $('#previewHeadingField').val().replace(/\s+/g, '_').toLowerCase() + '_4.' + getFilePathExtension($('#locationImageFour').val());

            self.controller.newItemAdded({
                isDiveSite: $('#diveSite option:selected').index() == 0 ? 1 : 0,
                heading: $('#previewHeadingField').val(),
                subheading: $('#previewSubheadingField').val(),
                thumbnailFile: $('#previewThumbnailFile').get(0).files[0],
                rating: $('#rating').find(':selected').text(),
                ratingComment: $('#ratingComment').val(),
                locationTextTitle: $('#locationText').val(),
                locationText: $('#locationText').val(),
                thumbnailPath: $('#previewHeadingField').val().replace(/\s+/g, '_').toLowerCase() + '.' + getFilePathExtension($('#previewThumbnailFile').val()),
                depth: $('#previewDepth').val(),
                visibility: $('#previewVisibility').val(),
                diverLevel: $('#diverLevelDropdown option:selected').index() + 1,
                diverCurrent: $('#diverCurrentDropdown option:selected').index(),
                locationImagePaths: '["' + pathOne + '", "' + pathTwo + '", "' + pathThree + '", "' + pathFour + '"]',    

                


                // These will need paths as well these paths go into the one string
                locationImageOne: $('#locationImageOne').get(0).files[0],
                locationImageTwo: $('#locationImageTwo').get(0).files[0],
                locationImageThree: $('#locationImageThree').get(0).files[0],
                locationImageFour: $('#locationImageFour').get(0).files[0]

            });

            // How are we going to work out a name for the thumbnail? A path?

            // Take the header, lowercase it, replace spaces with underscores.



        }

        $(saveButton).click(function() {
            save();
            // Let's assume this is a new one. Just for now.
        });

        $(submitButton).click(function() {
            var ajaxHeading = $('#previewHeadingField').val();
            var ajaxSubheading = $('#previewSubheadingField').val();
            var isDiveSite = $('#diveSite option:selected').index() == 0 ? 1 : 0;
            var locationText = $('#locationText').val();
            var diverLevel = $('#diverLevelDropdown option:selected').index() + 1;
            var depth = $('#previewDepth').val();
            var visibility = $('#previewVisibility').val();
            var diverCurrent = $('#diverCurrentDropdown option:selected').index();



            var formData = new FormData();
            formData.append('isDiveSite', isDiveSite);
            formData.append('heading', ajaxHeading);
            formData.append('subheading', ajaxSubheading);

            // How I get file info?
            formData.append('thumbnail', $('#previewThumbnailFile').get(0).files[0]);
            formData.append('rating', $('#rating').find(':selected').text());
            formData.append('ratingComment', $('#ratingComment').val());
            formData.append('preview', true);
            formData.append('thumbnail', thumbNailSrc);
            formData.append('locationText', locationText);
            formData.append('diverLevel', diverLevel);
            formData.append('depth', depth);
            formData.append('visibility', visibility);
            formData.append('current', diverCurrent);


            
            var locationImagePaths = '["' + locationImageOneSrc + '", "' + locationImageTwoSrc + '", "' + locationImageThreeSrc + '", "' + locationImageFourSrc + '"]';
            formData.append('locationImagePaths', locationImagePaths);

            $.ajax({
                url: '../divesite.php',
                type: 'POST',
                data: formData,
                success: function(data) {
                    placePreview.innerHTML = data;
                },
                cache: false,
                contentType: false,
                processData: false
            });
        });      
    });
    
   // listContainer.appendChild(newButton);



    for (var i = 0; i < this.model.places.length; i++) {
        // Add each list item here
        var listItem = document.createElement('li');
        listItem.classList.add('placeListItem');

        // Need the heading


        var listItemHeading = document.createElement('h1');
        listItemHeading.innerHTML = this.model.places[i].heading;
        
        var subheading = document.createElement('p');
        subheading.innerHTML = this.model.places[i].subheading;

        var thumbnailContainer = document.createElement('div');
        thumbnailContainer.classList.add('thumbnailContainer');

        var paddingTrickDiv = document.createElement('div');
        paddingTrickDiv.classList.add('paddingTrickDiv');
        thumbnailContainer.appendChild(paddingTrickDiv);

        var thumbnail = document.createElement('img');
        thumbnail.src = 'img/divesites/thumbnails/' + this.model.places[i].thumbnailPath;
        
        thumbnailContainer.appendChild(thumbnail);

        


        var editButton = document.createElement('button')
        editButton.classList.add('btn');
        editButton.classList.add('btn-primary');
        editButton.innerText = 'Edit';
        editButton.setAttribute('data-toggle', 'modal');
        editButton.setAttribute('data-target', '#newEditModal');


        var delButton = document.createElement('button');
        delButton.classList.add('btn');
        delButton.classList.add('btn-danger');
        delButton.innerText = 'Delete';
        delButton.setAttribute('data-toggle', 'modal');
        delButton.setAttribute('data-target', '#deleteModal');
        
        listItem.appendChild(listItemHeading);
        listItem.appendChild(subheading);
        listItem.appendChild(thumbnailContainer);
        listItem.appendChild(editButton);
        listItem.appendChild(delButton);
        var self = this;

        $(listItem).mousedown(function() {
            self.lastSourcePosition = $(this).index();
        });



        listContainer.appendChild(listItem);
    }


    

    

    $(listContainer).sortable();





    this.element.appendChild(listContainer);




    $(listContainer).on('sortstop', function(event, ui) {
        self.lastDestPosition = ui.item.index();
        self.controller.listOrderChanged(self.lastSourcePosition - 1, self.lastDestPosition - 1);
    });

};