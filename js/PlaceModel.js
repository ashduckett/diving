var PlaceModel = function(isDiveSite, heading, subheading, thumbnailPath, diverLevel, rating, ratingComment, depth, visibility, current, locationText, locationTextTitle, locationImagePaths, presentationOrder) {
    
    this.isDiveSite = isDiveSite;
    this.heading = heading;
    this.subheading = subheading;
    this.thumbnailPath = thumbnailPath;
    this.diverLevel = diverLevel;
    this.rating = rating;
    this.ratingComment = ratingComment;
    this.depth = depth;
    this.visibility = visibility;
    this.current = current;
    this.locationText = locationText;
    this.locationTextTitle = locationTextTitle;
    this.locationImagePaths = locationImagePaths;
    this.presentationOrder = presentationOrder;
    
};

PlaceModel.prototype.save = function(place) {
    // We want to add what's passed to the database.

    // This place should also be added to the collection.
};

var PlaceCollection = function(places) {
    this.places = [];

    for (var i = 0; i < places.length; i++) {
        // Fill this out
        var currentPlace = places[i];

        var newPlace = new PlaceModel();
    
        newPlace.id = currentPlace.id;
        newPlace.isDiveSite = currentPlace.isDiveSite;
        newPlace.heading = currentPlace.heading;
        newPlace.subheading = currentPlace.subheading;
        newPlace.thumbnailPath = currentPlace.thumbnailPath;
        newPlace.diverLevel = currentPlace.diverLevel;
        newPlace.rating = currentPlace.rating;
        newPlace.ratingComment = currentPlace.ratingComment;
        newPlace.depth = currentPlace.depth;
        newPlace.visibility = currentPlace.visibility;
        newPlace.current = currentPlace.current;
        newPlace.locationText = currentPlace.locationText;
        newPlace.locationTextTitle = currentPlace.locationTextTitle;
        newPlace.locationImagePaths = currentPlace.locationImagePaths;
        newPlace.presentationOrder = parseInt(currentPlace.presentationOrder);
        newPlace.virtualTourUrl = currentPlace.virtualTourUrl;
        newPlace.island = currentPlace.island;
        this.places.push(newPlace);
    }

    // Sort by presentation order

    this.places.sort(function(placeB, placeA) {
        return placeB.presentationOrder - placeA.presentationOrder;
    });

    console.log(this.places)
};

PlaceCollection.prototype.getPlaceByIndex = function(index) {
    return this.places[index];
};

PlaceCollection.prototype.update = function(arrayOfPlaces) {
    $.post('../utils/databaseTransactions.php', {data: JSON.stringify(arrayOfPlaces), transactionType: 'update', objectType: 'place'}, function(data) {
        console.log('success')
        console.log(data)
    });
};


// This is used to insert a new site
PlaceCollection.prototype.save = function(newPlace) {
    console.log(newPlace)
    var formData = new FormData();


  





    formData.append('isDiveSite', newPlace.isDiveSite);
    formData.append('heading', newPlace.heading);
    formData.append('subheading', newPlace.subheading);
    formData.append('thumbnailPath', newPlace.thumbnailPath);
    formData.append('locationTextTitle', newPlace.locationTextTitle);
    formData.append('locationText', newPlace.locationText);
    formData.append('rating', newPlace.rating);
    formData.append('ratingComment', newPlace.ratingComment);
    formData.append('depth', newPlace.depth);
    formData.append('visibility', newPlace.visibility);
    formData.append('current', newPlace.diverCurrent);
    formData.append('virtualTourUrl', 'ting');
    formData.append('diverLevel', newPlace.diverLevel);

    //formData.append('island', newPlace.island);
    formData.append('presentationOrder', this.places.length);
    formData.append('thumbnailFile', newPlace.thumbnailFile);
    formData.append('locationImagePaths', newPlace.locationImagePaths);
    formData.append('transactionType', 'save');
    formData.append('objectType', 'place');

    // formData.append('data', 'place');
    

    
    $.ajax({
        url: '../utils/databaseTransactions.php',
        // dataType: 'script',
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        type: 'post',
        success: function(data) {
            console.log('success')
            console.log(data)
        }
    });
 
    /*

     var file_data = $("#avatar").prop("files")[0]; // Getting the properties of file from file field
  var form_data = new FormData(); // Creating object of FormData class
  form_data.append("file", file_data) // Appending parameter named file with properties of file_field to form_data
  form_data.append("user_id", 123) // Adding extra parameters to form_data
  $.ajax({
    url: "/upload_avatar", // Upload Script
    dataType: 'script',
    cache: false,
    contentType: false,
    processData: false,
    data: form_data, // Setting the data attribute of ajax with file_data
    type: 'post',
    success: function(data) {
      // Do something after Ajax completes 
    }

    */

    //$.post('../utils/databaseTransactions.php', {data: JSON.stringify(newPlace), transactionType: 'save', objectType: 'place'}, function(data) {
    //    console.log('success')
    //    console.log(data)
    //});
};

PlaceCollection.prototype.swapPlaces = function(sourceIndex, destIndex) {
    var temp = this.places[sourceIndex];
    this.places[sourceIndex] = this.places[destIndex];
    this.places[destIndex] = temp;

    // Maintain presentation order for each
    this.places[sourceIndex].presentationOrder = sourceIndex;
    this.places[destIndex].presentationOrder = destIndex;

    this.update([this.places[sourceIndex], this.places[destIndex]]);
    // Send ajax request to save

    // After saving, the order in the admin view should respect reorders
};