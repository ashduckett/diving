function setupHoverEffect() {
    $('.site').mouseenter(function() {
        $(this).find('.darkeningOverlay').addClass('darkeningOverlayDarker');
    });

    $('.site').mouseleave(function() {
        $(this).find('.darkeningOverlay').removeClass('darkeningOverlayDarker');
    });
}

function isScrolledIntoView(elem) {
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}



function setupContactFormButtons() {
    $('.getInTouch').click(function(evt) {
        evt.preventDefault();
        var contactFormOverlay = document.querySelector('.contactFormOverlay');
        contactFormOverlay.style.display = 'block';
    
    
    
        // Add class here: leftSideForMac
        var mac = navigator.platform.match(/(Mac|iPhone|iPod|iPad)/i) ? true : false;
        
        if (mac) {
            document.getElementById('close').classList.add("left");
            $('.closeButton').addClass('leftSideForMac');
        }
    
    
    });

    var contactFormOverlay = document.querySelector('.contactFormOverlay');
    
    contactFormOverlay.addEventListener('click', function(evt) {
        this.style.display = 'none';
    });

    $('.closeButton').click(function() {
        contactFormOverlay.style.display = 'none';
    });




    var formDiv = document.querySelector('.formDiv');
    formDiv.addEventListener('click', function(evt) {
        evt.stopPropagation();
    });
}

function createSelectBox(selectId, options) {
    var label = document.createElement('label');
    label.classList.add('selectBoxLabelContainer');

    var select = document.createElement('select');
    select.classList.add('customSelectBox');
    select.id = selectId;
    select.classList.add('adminDropdown');


    for (var i = 0; i < options.length; i++) {
        var option = document.createElement('option');    
        option.innerText = options[i];
        select.appendChild(option);
    }
    label.appendChild(select);
    return label;
}


function setHeaderSize() {
    var width = $('.site').width();
    if(width < 400) {
        $('.site').find('h1').css('font-size', '1.5em');
    } else if(width < 500) {
        $('.site').find('h1').css('font-size', '2.5em');
    } else {
        $('.site').find('h1').css('font-size', '3em');
    }
}

function setupSiteClickThrough() {
    $('.site').click(function() {
        if (!$(this).hasClass('comingSoon')) {
            window.location = $(this).find("a").not('.comingSoon').attr("href");
            return false;
        }
    });
}

$(document).ready(function() {

    $('#contactUsEmail').click(function() {
        // console.log('clocked')

        // name, email, phone, location, message

        var name = $('#contactUsName').val();
        var email = $('#emailField').val();
        var phone = $('#phoneField').val();
        var location = $('#contactUsLocation').val();
        var message = $('#contactUsMessage').val();

        console.log('name', name);
        console.log('email', email);
        console.log('phone', phone);
        console.log('location', location);
        console.log('message', message);


        var obj = {
            name: $('#contactUsName').val(),
            email: $('#emailField').val(),
            phone: $('#phoneField').val(),
            location: $('#contactUsLocation').val(),
            message: $('#contactUsMessage').val()
        }


        $.post('../contact_us.php', obj, function(data) {
            console.log('I think we have success');
            console.log(data);
        });

    });

    $('.slickSlider').slick({
        //setting-name: setting-value
        arrows: true,
        // autoplay: true,
        adaptiveHeight: false
    });
    $.get('utils/getFilteredPlaces.php', {
    }).done(function(data) {
        var container = $('#divesitesAndPlaces');
        container.html(data);
        setupContactFormButtons();
        setupHoverEffect();
        setupSiteClickThrough();
    });

    $('#searchButton').click(function(evt) {
        evt.preventDefault();

        var searchTerm = $.trim($('#searchTerm').val());
        searchTerm = searchTerm == '' ? null : searchTerm;
        
        var islandValue = $('select#islandsDropdown').val();
        var categoryValue = $('#categoryDropdown').val();

        var region = $('#regionDropdown').val();
        var entry = $('#entryDropdown').val();
        var type = $('#typeDropdown').val();
        var region = $('#regionDropdown').val();
        var depthUpper = null;
        var depthLower = null;
        var depthIndex = parseInt($('#depthDropdown').val());

        if (depthIndex) {
            switch(depthIndex) {
                case 1:
                    depthLower = 0;
                    depthUpper = 15;
                    break;
                case 2:
                    depthLower = 16;
                    depthUpper = 30;
                    break;
                case 3:
                    depthLower = 31;
                    depthUpper = 45;
                    break;
                case 4:
                    depthLower = 46;
                    depthUpper = 60;
                    break;
                case -1:
                    depthLower = -1;
                    depthUpper = -1;
                    break;

                default:
                    depthLower = null;
                    depthUpper = null;
                    break;

            }
        } else {
            depthLower = -1;
            depthUpper = -1;
        }

        var current = $('#currentDropdown').val();
        var level = $('#levelDropdown').val();

        $.get('utils/getFilteredPlaces.php', { 
            searchTerm: searchTerm, 
            island: islandValue,
            category: categoryValue,
            depthLower: depthLower,
            depthUpper: depthUpper,
            current: current,
            level: level,
            region: region,
            isPaid: entry,
            type: type
        }).done(function(data) {
            var container = $('#divesitesAndPlaces');
            container.html(data);
            setupContactFormButtons();
            setupHoverEffect();
            setupSiteClickThrough();
        });
    });





    $('#islandsDropdown').hover(function() {

        $('#islandsDropdown+ul').fadeIn('fast');
        $('#menuCarat').css('opacity', '1');
        $('#islandsDropdown').css('opacity', '0.75');
    });

    $('.menu').hover(function() {}, function() {
        $('#islandsDropdown+ul').fadeOut('fast');
        //$('#menuCarat').fadeOut('fast');
        $('#menuCarat').css('opacity', '0');
        $('#islandsDropdown').css('opacity', '1');
    });
    
    $('#desktopHome, #desktopAbout').hover(function() {
        $('#islandsDropdown+ul').fadeOut('fast');
        //$('#menuCarat').fadeOut('fast');
        $('#menuCarat').css('opacity', '0');
        $('#islandsDropdown').css('opacity', '1');


    }, function() {
        
    });
    
    //$("ul").slideDown("slow");
    setHeaderSize();

    $('.hamburger').click(function() {
        $(this).toggleClass('is-active');
       
             
        
        
        
    //   $('.mobile-menu').toggleClass('forwardAnimation');
    // $('.mobile-menu').css('display', 'block');


        // if ($('.mobile-menu').css('opacity') == 0) {
        //     console.log('showing')

        //     $('.mobile-menu').css('opacity', 1);
        //     $('.mobile-menu').css('pointer-events', 'all');
        // } else {
        //     console.log('hiding')
        //     $('.mobile-menu').css('opacity', 0);
        //     $('.mobile-menu').css('pointer-events', 'none');
        // } 

        $('.mobile-menu').toggleClass('fullOpacity');

            




       // $('.mobile-menu').toggleClass('reverseAnimation');

    //     $('.mobile-menu').fadeToggle(200);
    //    $('.mobile-menu').toggleClass('displayed');
        
        
     
         $('nav').toggleClass('mobile-menu-open');
    });

    $('#categoryDropdown').change(function() {
        var index = $("#categoryDropdown option:selected").index()


        if (index == 1) {
        
            $('.depthSelect').html('');
            var newSelectControl = createSelectBox('depthDropdown', ['Depth', '0 - 15m', '16 - 30m', '31 - 45m', '46 - 60m']);
            $('.depthSelect').append(newSelectControl);

            $('.currentSelect').html('');
            var newSelectControl = createSelectBox('currentDropdown', ['Current', 'Weak', 'Normal', 'Average', 'Strong', 'Extra Strong']);
            $('.currentSelect').append(newSelectControl);

            // Level
            $('.levelSelect').html('');
            var newSelectControl = createSelectBox('levelDropdown', ['Level', '1', '2', '3', '4', '5']);
            $('.levelSelect').append(newSelectControl);

            // Rebuild islands drop down
           // var newSelectControl = createSelectBox('islandsDropdown', ['Islands', 'Koh Tao', 'Koh Samui', 'Koh Chang']);

           $('.islandsSelect').show();

        } else if (index == 2) {
            $('.depthSelect').html('');
            var newSelectControl = createSelectBox('regionDropdown', ['Region', 'Central Thailand', 'Eastern Thailand', 'Northern Thailand', 'Northeastern Thailand', 'Southern Thailand', 'Western Thailand']);
            $('.depthSelect').append(newSelectControl);

            console.log('you have selected places to go')


            $('.currentSelect').html('');
            var newSelectControl = createSelectBox('entryDropdown', ['Entry', 'Free Entry', 'Paid']);
            $('.currentSelect').append(newSelectControl);
            // What else needs removing and replacing?

            // Level
            $('.levelSelect').html('');
            var newSelectControl = createSelectBox('typeDropdown', ['Type', 'Historical', 'Religious', 'Culture', 'Nature', 'Natural Wonders', 'Wildlife']);
            $('.levelSelect').append(newSelectControl);

            $('.islandsSelect').hide();
            



        }

        
    });
});

window.addEventListener('resize', function() {
    setHeaderSize();

});



window.addEventListener('scroll', function() {
   // Once we get to around 400, change to have a fuller background
    if ($(window).scrollTop() > 650) {
       // $('nav').css('position', 'fixed');

        $('nav').addClass('navFixed');

        $('.menuGradient').addClass('fullerBackgroundGradient');
    } else {
       // $('nav').css('position', 'relative');
        $('nav').removeClass('navFixed');
        $('.menuGradient').removeClass('fullerBackgroundGradient');
    }


    // Change width of bars!

    var meters = $('img.meter.lightenedMeter');

    var metersWithScrolledStatus = [];

    



    for (var i = 0; i < meters.length; i++) {
        if(isScrolledIntoView(meters[i])) {
            if(!meters[i].hasBeenScrolled) {
                var widthToBe = $(meters[i]).attr("data-width-to-be");
                $(meters[i]).css('width', widthToBe);
                meters[i].hasBeenScrolled = true;
            }
        }
    }

    // Now for the lights
    
    // Grab the containers
    


    function animate(lights) {
        var currentIndex = 0;
        var noOfLights = lights.length - 1;
        
        var timerId = setInterval(function() { 
            $(lights).removeClass('grow');
            if ($(lights[currentIndex]).hasClass('active')) {
                $(lights[currentIndex]).addClass('grow');
            }
            
            currentIndex++;

            if (currentIndex > noOfLights) {
                currentIndex = 0;
                clearInterval(timerId);
            }
        }, 
        1000);
    }
    var lightContainers = $('.lightLevelContainer');

        lightContainers.each(function(item) {
            if(isScrolledIntoView($(this))) {
                if(!lightContainers[item].hasBeenScrolled) {
                    lightContainers[item].hasBeenScrolled = true;

                    // What about only passing in the active
                    animate($(this).find('img.light.active'));

                    // Calculate an interval based on the number of lights:
                    var interval = ($(this).find('img.light.active').length + 2) * 1000;
                       
                    var self = $(this);
                        setInterval(function() {
                        self.find('img.light').removeClass('grow');
                        animate(self.find('img.light.active'));
                    }, interval);
                }
            }


        });
});