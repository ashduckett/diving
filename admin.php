<?php require_once "database/DiveSite.php"; ?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
  
    <link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/jquery-ui.css"/>    

    <script src="js/jquery-3.3.1.js"></script>
   
    <script src="js/ofi.min.js"></script>
    <script src="js/utils.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/Utils.js"></script>
    <script src="js/PlaceModel.js"></script>
    <script src="js/PlaceView.js"></script>
    <script src="js/PlaceController.js"></script>
    <link rel="stylesheet" href="css/admin.css">
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">


    
    


    <title>Admin</title>
</head>
<body>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Dive Thailand</a>
            </div>
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Places</a></li>
                <li><a href="#">Users</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a id="newButton" href="#">New Place</a></li>
            </ul>
        </div>
    </nav>

    <div class="listView"></div>

    <script>
        var places = <?php echo DiveSite::getAllAsJSON(); ?>;
        var placeModel = new PlaceCollection(places);
        var placeView = new PlaceView();

        // On initially loading, render the places view
        var container = document.querySelector('.listView');
        var placeController = new PlaceController(container, placeModel, placeView);
        placeController.render();





    </script>








    <!-- Deletion Modal -->
    <!-- Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Confirm</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Are you sure you want to delete this place?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-danger">Delete</button>
      </div>
    </div>
  </div>
</div>

<!-- New/Edit place form -->
<div class="modal fade" id="newEditModal" tabindex="-1" role="dialog" aria-labelledby="newEditModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle"><span id="newEditTitle"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
            <div class="form-group">
                <label for="exampleFormControlInput1">Place Type</label>
                <select class="form-control" id="placeType">
                    <option>Dive Site</option>
                    <option>Place to Go</option>
                </select>
            </div>
            <div class="form-group">
                <label for="header">Header</label>
                <input type="text" class="form-control" id="header" aria-describedby="placeHeader" placeholder="Header">
                <small id="emailHelp" class="form-text text-muted">This will appear both in the thumbnail and the place specific page</small>
            </div>
            <div class="form-group">
                <label for="subheader">Sub-header</label>
                <input type="text" class="form-control" id="subheader" aria-describedby="placeSubHeader" placeholder="Subheader">
                <small id="subHeaderHelp" class="form-text text-muted">This will appear both in the thumbnail and the place specific page</small>
            </div>
            <div class="custom-file">
                <label class="custom-file-label" for="customFile">Thumbnail/Page Header Image</label>    
                <input type="file" class="custom-file-input" id="thumbnailFile">
                <small id="subHeaderHelp" class="form-text text-muted">The image selected here will be used as the thumbnail and header</small>
            </div>
            <div class="form-group">
                <label for="header">Description Header</label>
                <input type="text" class="form-control" id="locationTextTitle" aria-describedby="placeSubHeader" placeholder="Subheader">
                <small id="subHeaderHelp" class="form-text text-muted">This will appear above the description entered for this place</small>
            </div>
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Description</label>
                <textarea class="form-control" id="locationText" rows="3"></textarea>
                <small id="descriptionHelp" class="form-text text-muted">Shown above the set of images for the area</small>
            </div>
            <div class="custom-file">
                <label class="custom-file-label" for="customFile">Primary Image</label>    
                <input type="file" class="custom-file-input" id="customFile">
                <small id="subHeaderHelp" class="form-text text-muted">The image selected here will be the initially large image in your image presentation</small>
            </div>
            <div class="custom-file">
                <label class="custom-file-label" for="customFile">Secondary Image</label>    
                <input type="file" class="custom-file-input" id="customFile">
                <small id="subHeaderHelp" class="form-text text-muted">Shown along the bottom and a selectable item of the presentation</small>
            </div>
            <div class="custom-file">
                <label class="custom-file-label" for="customFile">Secondary Image</label>    
                <input type="file" class="custom-file-input" id="customFile">
                <small id="subHeaderHelp" class="form-text text-muted">Shown along the bottom and a selectable item of the presentation</small>
            </div>
            <div class="custom-file">
                <label class="custom-file-label" for="customFile">Secondary Image</label>    
                <input type="file" class="custom-file-input" id="customFile">
                <small id="subHeaderHelp" class="form-text text-muted">Shown along the bottom and a selectable item of the presentation</small>
            </div>
            <div class="form-group">
                <label for="exampleFormControlInput1">Diver Level</label>
                <select class="form-control" id="diverLevel">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                </select>
                <small id="subHeaderHelp" class="form-text text-muted">Select the minimum level a diver must be at in order to participate in this dive</small>
            </div>
            <div class="form-group">
                <label for="header">Depth</label>
                <input type="text" class="form-control" id="depth" aria-describedby="placeSubHeader" placeholder="Depth">
                <small id="subHeaderHelp" class="form-text text-muted">Enter the depth of this dive in metres</small>
            </div>
            <div class="form-group">
                <label for="header">Visibility</label>
                <input type="text" class="form-control" id="visibility" aria-describedby="placeSubHeader" placeholder="Visibility">
                <small id="subHeaderHelp" class="form-text text-muted">Enter the visibility of this dive in metres</small>
            </div>
            <div class="form-group">
                <label for="exampleFormControlInput1">Current Strength</label>
                <select class="form-control" id="currentStrength">
                    <option>Weak</option>
                    <option>Normal</option>
                    <option>Average</option>
                    <option>Strong</option>
                    <option>Extra Strong</option>
                </select>
                <small id="subHeaderHelp" class="form-text text-muted">Select the minimum level a diver must be at in order to participate in this dive</small>
            </div>
            <div class="form-group">
                <label for="header">Virtual Tour Latitude</label>
                <input type="text" class="form-control" id="header" aria-describedby="placeHeader" placeholder="Header">
                <small id="emailHelp" class="form-text text-muted">When provided with a longitude, this value will provide a virtual tour of the area where possible</small>
            </div>
            <div class="form-group">
                <label for="header">Virtual Tour Longitude</label>
                <input type="text" class="form-control" id="header" aria-describedby="placeHeader" placeholder="Header">
                <small id="emailHelp" class="form-text text-muted">When provided with a latitude, this value will provide a virtual tour of the area where possible</small>
            </div>
            <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" id="saveButton" class="btn btn-primary">Save</button>
      </div>
    </div>
  </div>
</div>









</body>
</html>