<?php 

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


require_once('utils/functions.php'); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>
    <link rel="stylesheet" type="text/css" href="css/slider.css"/>
    <link rel="stylesheet" type="text/css" href="css/ScreenSlider.css"/>
    <link rel="stylesheet" type="text/css" href="css/jquery-ui.css"/>
    
    <script src="js/jquery-3.3.1.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/ofi.min.js"></script>
    <script src="js/utils.js"></script>
    <script src="js/videoSplit.js"></script>
    <script src="js/ScreenSlider.js"></script>
    <script type="text/javascript" src="slick/slick.js"></script>
    <title>Thailand Dive</title>
</head>

<body>
    <div class="contactFormOverlay">
        <div class="formDiv">
            <div class="formBanner">
                <div class="formImageContainer">
                    <img src="img/contact_form.jpg">
                </div>
                <div class="closeButton"><i class="fas fa-times"></i></div>
                <!-- <div class="formHeaderCentral"><h1 class="largeHeader">Find the perfect dive site for you</h1></div>
                <div class="formHeaderDarkener"></div> -->
            </div>
            <div class="formHeader">
                <!-- public static function getStandardCenteredSection($header, $text, Array $buttons, $textColour = null, $backgroundColour = null, $extraClasses = null) { -->
                <section class="standardCenteredSection">        
                    <h2 class="mediumHeader formHeaderText">The Best Dive Spots in Thailand from Fully Qualified Divers</h2>
                    <p class="standardSectionText contactFormText">
                        Dive Thailand is an organisation fuelled by professional divers based within the U.K. We’re committed in aiding divers
                        looking at Thailand and its surrounding islands as their next diving destination. Helping divers know where to go
                        where to dive and where to stay.
                    </p>        
                </section>
            </div>
            <div class="contactFormGuts">
                <div class="fieldRow">
                    <div class="fieldContainer">
                        <div class="formField">
                            <label for="contactUsName">Name*</label>
                            <input id="contactUsName" name="name" type="text">
                        </div>
                    </div>
                    <div class="fieldContainer">
                        <div class="formField">
                            <label for="emailField">Email*</label>
                            <input id="emailField" name="name" type="text">
                        </div>
                    </div>
                </div>
                <div class="fieldRow">
                    <div class="fieldContainer">
                        <div class="formField">
                            <label for="nameField">Phone*</label>
                            <input id="phoneField" name="name" type="text">
                        </div>
                    </div>
                </div>
                <div class="fieldRow">
                    <div class="fieldContainer">
                        <div class="formField">
                            <label for="nameField">Location*</label>
                            <input id="contactUsLocation" name="name" type="text" placeholder="i.e. Glocestershire">
                        </div>
                    </div>
                </div>
                <div class="fieldRow">
                    <div class="fieldContainer">
                        <div class="formField">
                            <label for="nameField">Message*</label>
                            <textarea id="contactUsMessage" name="name"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fieldRow submitFormRow">
                <div class="fieldContainer">
                    <div class="formField">
                        <a id="contactUsEmail" class="btn btn-dark-blue" href="#">Submit</a>
                    </div>
                </div>
            </div>
        
        </div>
    </div>