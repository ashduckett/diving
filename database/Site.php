<?php
    // Check this out, it looks like it might make things easy: https://phpdelusions.net/pdo/objects


    abstract class Site {
        private $id;
        private $heading;
        private $subHeading;
        private $thumbnailPath;
        private $rating;
        private $ratingComment;
        private $locationText;
        private $locationTextTitle;
        private $island;
        private $presentationOrder;
    }