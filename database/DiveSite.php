<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once "DBObject.php";
    require_once 'config.php';

    class DiveSite extends DataObject implements JSONSerializable {
        protected $data = array(
            "id" => "",                                 // common
            "heading" => "",                            // common
            "subheading" => "",                         // common
            "thumbnailPath" => "",                      // common
            "diverLevel" => "",                         // dive site
            "rating" => "",                             // common
            "ratingComment" => "",                      // common
            "depth" => "",                              // dive site
            "visibility" => "",                         // dive site
            "current" => "",                            // dive site
            "locationText" => "",                       // common
            "locationTextTitle" => "",                  // common
            "locationImagePaths" => "",                 // dive site
            "islands" => "",                            // common
            "virtualTourUrl" => "",                     // place to go
            "isDiveSite" => "",                         // would this be needed?
            "presentationOrder" => ""                   // common
        );

        // This will need to be updated for a full insert.
         public function insert() {
            $conn = DataObject::connect();
            $sql = "INSERT INTO " . TBL_DIVE_SITE . "(heading, subheading, thumbnailPath, diverLevel, rating, ratingComment, depth, visibility, 
                                                        current, locationText, locationTextTitle, locationImagePaths, islands, virtualTourUrl,
                                                        isDiveSite, presentationOrder) VALUES (:heading, :subheading, :thumbnailPath, :diverLevel, 
                                                        :rating, :ratingComment, :depth, :visibility, :current, :locationText, :locationTextTitle, 
                                                        :locationImagePaths, :island, :virtualTourUrl, :isDiveSite, :presentationOrder)";
            
            
            try {
                $st = $conn->prepare($sql);
                $st->bindValue(":heading", $this->data["heading"], PDO::PARAM_STR);
                $st->bindValue(":subheading", $this->data["subheading"], PDO::PARAM_STR);
                $st->bindValue(":thumbnailPath", $this->data["thumbnailPath"], PDO::PARAM_STR);
                $st->bindValue(":diverLevel", $this->data["diverLevel"], PDO::PARAM_INT);
                $st->bindValue(":rating", $this->data["rating"], PDO::PARAM_STR);
                $st->bindValue(":ratingComment", $this->data["ratingComment"], PDO::PARAM_STR);
                $st->bindValue(":depth", $this->data["depth"], PDO::PARAM_STR);
                $st->bindValue(":visibility", $this->data["visibility"], PDO::PARAM_STR);
                $st->bindValue(":current", $this->data["current"], PDO::PARAM_STR);
                $st->bindValue(":locationText", $this->data["locationText"], PDO::PARAM_STR);
                $st->bindValue(":locationTextTitle", $this->data["locationTextTitle"], PDO::PARAM_STR);
                $st->bindValue(":locationImagePaths", $this->data["locationImagePaths"], PDO::PARAM_STR);
                $st->bindValue(":islands", $this->data["islands"], PDO::PARAM_STR);
                $st->bindValue(":virtualTourUrl", $this->data["virtualTourUrl"], PDO::PARAM_STR);
                $st->bindValue(":isDiveSite", $this->data["isDiveSite"], PDO::PARAM_INT);
                $st->bindValue(":presentationOrder", $this->data["presentationOrder"], PDO::PARAM_INT);
                $st->execute();
                $lastInsertId = $conn->lastInsertId();
                DataObject::disconnect($conn);
            } catch(PDOException $e) {
                die("Query failed: " . $e->getMessage());
            }
            return $lastInsertId;
        }

        public function update() {
            $conn = DataObject::connect();

            $sql = 'UPDATE ' . TBL_DIVE_SITE . ' SET heading = :heading, subheading = :subheading, thumbnailPath = :thumbnailPath, diverLevel = :diverLevel, rating = :rating, 
                ratingComment = :ratingComment, depth = :depth, visibility = :visibility, current = :current, locationText = :locationText, locationTextTitle = :locationTextTitle, 
                locationImagePaths = :locationImagePaths, island = :island, virtualTourUrl = :virtualTourUrl, isDiveSite = :isDiveSite, presentationOrder = :presentationOrder WHERE id = :id;';

            try {
                $st = $conn->prepare($sql);
                $st->bindValue(":id", $this->data['id']);
                $st->bindValue(":heading", $this->data['heading']);
                $st->bindValue(":subheading", $this->data['subheading']);
                $st->bindValue(":thumbnailPath", $this->data['thumbnailPath']);
                $st->bindValue(":diverLevel", $this->data['diverLevel']);
                $st->bindValue(":rating", $this->data['rating']);
                $st->bindValue(":ratingComment", $this->data['ratingComment']);
                $st->bindValue(":depth", $this->data['depth']);
                $st->bindValue(":visibility", $this->data['visibility']);
                $st->bindValue(":current", $this->data['current']);
                $st->bindValue(":locationText", $this->data['locationText']);
                $st->bindValue(":locationTextTitle", $this->data['locationTextTitle']);
                $st->bindValue(":locationImagePaths", $this->data['locationImagePaths']);
                $st->bindValue(":island", $this->data['island']);
                $st->bindValue(":virtualTourUrl", $this->data['virtualTourUrl']);
                $st->bindValue(":isDiveSite", $this->data['isDiveSite']);
                $st->bindValue(":presentationOrder", $this->data['presentationOrder']);
                $st->execute();
                return true;
            } catch(PDOException $e) {
                die("Query failed: " . $e->getMessage());
            }
            
            
        }

        public static function getAll() {
            $conn = parent::connect();

            $sql = "SELECT * FROM " . TBL_DIVE_SITE;

            try {
                $st = $conn->prepare($sql);
                $st->execute();

                $diveSites = array();

                foreach($st->fetchAll() as $row) {
                    $diveSites[] = new DiveSite($row);
                }
           
                parent::disconnect($conn);
                return $diveSites;
            } catch(PDOException $e) {
                die("Query failed: " . $e->getMessage());
            }
        }

        public static function getFiltered($searchTerm, $islands, $diveSite, $lowerDepth, $upperDepth, $current, $level, $region, $isPaid, $type) {
  
            $conn = parent::connect();

            $sql = "SELECT * FROM " . TBL_DIVE_SITE . ' WHERE id IS NOT NULL ';

            if ($searchTerm != null) {
                $sql .= 'AND (LOWER(heading) LIKE LOWER(:searchTerm) || LOWER(subheading) LIKE LOWER(:searchTerm) || LOWER(locationText) LIKE LOWER(:searchTerm) || LOWER(locationTextTitle) LIKE LOWER(:searchTerm))';
            }

            if ($islands !== null) {
                $sql .= ' AND LOWER(islands) LIKE LOWER(:islands)';
            }

            if ($diveSite !== null) {
                $sql .= ' AND isDiveSite = :diveSite';
            }

            if($lowerDepth !== null && $upperDepth !== null) {
                $sql .= ' AND depth >= :lowerDepth AND depth <= :upperDepth';
            }

            if ($current !== null) {
                $sql .= ' AND current = :current';
            }
            
            if ($level !== null) {
                $sql .= ' AND diverLevel = :level';
            }

            if ($region !== null) {
                $sql .= ' AND LOWER(regions) LIKE LOWER(:region)';
            }

            if ($type !== null) {
                $sql .= ' AND LOWER(type) LIKE LOWER(:type)';
            }

            if ($isPaid !== null) {
                $sql .= ' AND isPaid = :isPaid';
            }

            $sql .= ' ORDER BY presentationOrder';
            try {
                $st = $conn->prepare($sql);
                $searchTerm = '%' . $searchTerm . '%';
                
                if ($region !== null) {
                    $region = '%' . $region . '%';
                }

                if ($type !== null) {
                    $type = '%' . $type . '%';
                }

                if ($islands !== null) {
                    $islands = '%' . $islands . '%';
                }

                if(strlen($searchTerm) > 2) {
                    $st->bindValue(":searchTerm", $searchTerm, PDO::PARAM_STR);
                }
                
                if($islands !== null) {
                    $st->bindValue(":islands", $islands, PDO::PARAM_STR);
                }


                if($diveSite !== null) {
                    $st->bindValue(":diveSite", $diveSite, PDO::PARAM_STR);
                }

                if($current !== null) {
                    $st->bindValue(":current", $current, PDO::PARAM_INT);
                }

                if($level !== null) {
                    $st->bindValue(":level", $level, PDO::PARAM_INT);
                 }

                 if($isPaid !== null) {
                    $st->bindValue(":isPaid", $isPaid, PDO::PARAM_INT);
                 }


                if($lowerDepth !== null && $upperDepth !== null) {
                    $st->bindValue(":lowerDepth", $lowerDepth, PDO::PARAM_INT);
                    $st->bindValue(":upperDepth", $upperDepth, PDO::PARAM_INT);
                }

                if ($region !== null) {
                    $st->bindValue(":region", $region, PDO::PARAM_STR);
                }

                
                if ($type !== null) {
                    $st->bindValue(":type", $type, PDO::PARAM_STR);
                }



                $st->execute();
            } catch (PDOException $e) {
                die("Query failed: " . $e->getMessage());
            }

            $diveSites = [];

            foreach($st->fetchAll() as $row) {
                $diveSites[] = new DiveSite($row);
            }
          
            return $diveSites;
        }

        public static function getAllAsJSON() {
            $conn = parent::connect();

            $sql = "SELECT * FROM " . TBL_DIVE_SITE;

            try {
                $st = $conn->prepare($sql);
                $st->execute();

                $diveSites = array();

                foreach($st->fetchAll() as $row) {
                    $diveSites[] = new DiveSite($row);
                }
           
                parent::disconnect($conn);
                return json_encode($diveSites);
            } catch(PDOException $e) {
                die("Query failed: " . $e->getMessage());
            }
        }

        public static function findById($diveSiteId) {
            $conn = DataObject::connect();
            $sql = "SELECT * FROM " . TBL_DIVE_SITE . " WHERE id = :diveSiteId";
            $st = $conn->prepare($sql);
            $st->bindValue(":diveSiteId", $diveSiteId);
            $st->execute();
            $diveSites = array();
                    
            foreach($st->fetchAll() as $row) {
                $diveSites[] = new DiveSite($row);
            }
            parent::disconnect($conn);
            
            return $diveSites[0];
       }

       public function jsonSerialize() {
            return $this->data;
        }



    }


?>
