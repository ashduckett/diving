    
    
    <?php require_once 'partials/header.php'; ?>
    <?php require_once 'database/DiveSite.php'; ?>
    <?php require_once 'strings.php'; ?>

    <?php
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
?>    

    <header>
        <div class="menuGradient"></div>
        <div class="doubleViewHeading heading">
            <div class="headerImages">
                <div class=" content navContainer">
                    <?php echo Utils::getNav(); ?>
                </div>
                <div class="hoverImageContainer leftBox">
                    <img class="headerImage" src="img/header_left.jpg">
                    <article>
                        <div class="clickThrough">
                            <h1 class="largeHeader">Dive with Us</h1>
                            <p class="standardSectionText">Let us show you the best diving spots in Thailand.</p>
                            <a class="btn btn-green" href="dive_hub.php">Dive</a>
                        </div>
                    </article>
                </div>
                <div class="hoverImageContainer rightBox">
                    <img class="headerImage" src="img/places_hub - 2.jpg">
                    <article>
                        <div class="clickThrough">
                            <h1 class="largeHeader">Where to go</h1>
                            <p class="standardSectionText">Discover Thailand for the places you and your diving buddies will love.</p>
                            <a class="btn btn-purple" href="places_to_go_hub.php">Explore</a>
                        </div>
                    </article>
                </div>
            </div>
        </div>

        <div class="singleViewHeading heading">
            <div class="headerImages">
                <div class="content navContainer">
                    <?php echo Utils::getNav(); ?>
                </div>
                <div class="hoverImageContainer ignoreHover leftBox" style="width:100%;">
                    <img class="headerImage" src="img/single_header.jpg">
                    <div class="stackedHeaderImages">
                        <article class="homepageTopArticle">
                            <div class="clickThrough">
                                <h1 class="largeHeader">Dive with Us</h1>
                                <p class="standardSectionText">Let us show you the best diving spots in Thailand.</p>
                                <a class="btn btn-green" href="dive_hub.php">Dive</a>
                            </div>
                        </article>
                        <article class="homepageBottomArticle">
                            <div class="clickThrough">
                                <h1 class="largeHeader">Where to go</h1>
                                <p class="standardSectionText">Discover Thailand for the places you and your diving buddies will love.</p>
                                <a class="btn btn-purple" href="places_to_go_hub.php">Explore</a>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <?php
        echo Utils::getStandardCenteredSection(HOME_BEST_SPOTS_HDR, HOME_BEST_SPOTS_TXT, []);
    ?>

    <div class="locationsMapContainer">
        <div class="locationsMapInlineContainer">
            <img id="locationsMap" src="img/map.png">
            <img class="mapDot" src="img/Level 1 On.png">
            <img class="mapDot" src="img/Level 1 On.png">
            <img class="mapDot" src="img/Level 1 On.png">
            <img class="mapDot" src="img/Level 1 On.png">
            <img class="mapDot" src="img/Level 1 On.png">
        </div>
    </div>

    <?php
        // Now get search and learn more buttons
        $searchBtn = new Button('Search', 'purple', '#');
        $learnMoreBtn = new Button('Learn More', 'green', '#');
        echo Utils::getStandardCenteredSection(HOME_GREAT_LOCATIONS_HDR, HOME_GREAT_LOCATIONS_TXT, [$searchBtn, $learnMoreBtn], 'White', 'DarkBlue', ['greatLocations']);
    
    
    ?>

    <?php Utils::getOverallSearchComponent('Search our database for your perfect dive in Thailand', 'Dive Thailand’s database will give you the chance to 
        get the low down on finding your next diving destination, with insights into visibility, depth and most enticing features.', 'White', 'Yellow'); ?>
    <?php require_once 'partials/footer.php'; ?>

<script>
    $.get('utils/getFilteredPlaces.php', {
    }).done(function(data) {
        var container = $('#divesitesAndPlaces');
        container.html(data);
        setupContactFormButtons();
        setupHoverEffect();
        setupSiteClickThrough();
    });

    // $('#searchButton').click(function(evt) {
    //     evt.preventDefault();

    //     var searchTerm = $.trim($('#searchTerm').val());
    //     searchTerm = searchTerm == '' ? null : searchTerm;
        
    //     var islandValue = $('select#islandsDropdown').val();
    //     var categoryValue = $('#categoryDropdown').val();
    //     var region = $('#regionDropdown').val();
    //     var entry = $('#entryDropdown').val();
    //     var type = $('#typeDropdown').val();
    //     var region = $('#regionDropdown').val();



        
    //     var depthUpper = null;
    //     var depthLower = null;
    //     if (depthIndex) {
    //     var depthIndex = parseInt($('#depthDropdown').val());
            
            
    //         switch(depthIndex) {
    //             case 1:
    //                 depthLower = 0;
    //                 depthUpper = 15;
    //                 break;
    //             case 2:
    //                 depthLower = 16;
    //                 depthUpper = 30;
    //                 break;
    //             case 3:
    //                 depthLower = 31;
    //                 depthUpper = 45;
    //                 break;
    //             case 4:
    //                 depthLower = 46;
    //                 depthUpper = 60;
    //                 break;
    //             case -1:
    //                 depthLower = -1;
    //                 depthUpper = -1;
    //                 break;

    //             default:
    //                 depthLower = null;
    //                 depthUpper = null;
    //                 break;

    //         }
    //     } else {
    //         depthLower = -1;
    //         depthUpper = -1;
    //     }

    //     var current = $('#currentDropdown').val();
    //     console.log('current is ' + current);
    //     console.log('island is ' + islandValue)

    //     var level = $('#levelDropdown').val();//find(":selected").text();
    //     //level = level == 'Level' ? null : level;
    //     console.log('we are posting an island value of ' + islandValue)
    //     $.get('utils/getFilteredPlaces.php', { 
          
    //     }).done(function(data) {
    //         var container = $('#divesitesAndPlaces');
    //         container.html(data);
    //         setupContactFormButtons();
    //         setupHoverEffect();
    //         setupSiteClickThrough();
    //     });
    // });

    // Keep track of the last dot that animated
    var oldDotIndex = null;
    var randomIndex = null;
function randomIntFromInterval(min,max)
{
    return Math.floor(Math.random()*(max-min+1)+min);
}

    // Set the dots off
    setInterval(function() {
        var dots = document.querySelectorAll('.mapDot');

        do {
            randomIndex = randomIntFromInterval(0, 4);
        } while (oldDotIndex == randomIndex);

        oldDotIndex = randomIndex;

        for (var i = 0; i < dots.length; i++) {
                dots[i].classList.remove('grow');
        }

        dots[randomIndex].classList.add('grow');
    }, 1500);


  


     
    
    
</script>