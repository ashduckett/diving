<?php
    require_once '../database/DiveSite.php';

    function ensureHasValue($getRequestKey) {
        if (isset($_GET[$getRequestKey])) {
         //   echo 'checking ' . $getRequestKey . ' which is ' . $_GET[$getRequestKey];

            return $_GET[$getRequestKey] == -1 ? null : $_GET[$getRequestKey];
        } else {
            return null;
        }
    }
    
    $searchTerm = ensureHasValue('searchTerm');
    $depthLower = ensureHasValue('depthLower');
    $depthUpper = ensureHasValue('depthUpper');

    echo $depthUpper;
    echo $depthLower;
    $current = ensureHasValue('current'); 
    $island = ensureHasValue('island');
    $isPaid = ensureHasValue('isPaid');    
    $region = ensureHasValue('region');
    $level = ensureHasValue('level');
    $type = ensureHasValue('type');
    $diveSite = ensureHasValue('category');

    $filteredDivesites = DiveSite::getFiltered($searchTerm, $island, $diveSite, $depthLower, $depthUpper, $current, $level, $region, $isPaid, $type);

    if (count($filteredDivesites) > 0) {
        echo '
            <section class="diveSites">';

            foreach ($filteredDivesites as $diveSite) {

                $colourClass = $diveSite->getValue('isDiveSite') == 1 ? 'btn-green' : 'btn-purple';
                $buttonText = $diveSite->getValue('isDiveSite') == 1 ? 'Dive' : 'Explore';

                echo '
                    <div class="site">
                        <div class="paddingTrick"></div>
                        <img class="islandImage" src="img/divesites/thumbnails/' . $diveSite->getValue('thumbnailPath') . '">
                        <div class="darkeningOverlay"></div>
                        <div class="siteTextOverlay">
                            <h1 class="largeHeader">' . $diveSite->getValue('heading') . '</h1>
                            <p class="standardSectionText">' . $diveSite->getValue('subheading') . '</p>
                            <a href="divesite.php?id=' . $diveSite->getValue('id') . '" class="btn ' . $colourClass . '">' . $buttonText . '</a>
                        </div>
                    </div>'
                    ;
            }

            echo '
                <div class="site comingSoon">
                    <div class="paddingTrick"></div>
                    <div class="siteTextOverlay">
                        <h1 class="largeHeader">Coming Soon</h1>
                        <p class="standardSectionText">Want us to add a location? Why not get in touch to add your perfect dive site or location?</p>
                        <a id="getInTouch" href="#" class="btn btn-light-red getInTouch">Get in touch</a>
                    </div>
                </div>
            </section>';
    } else {
        echo '<section class="diveSites">';
        for ($count = 0; $count <= 2; $count++) {
            echo '
                <div class="site comingSoon">
                <div class="paddingTrick"></div>
                    <div class="siteTextOverlay">
                        <h1 class="largeHeader">Coming Soon</h1>
                        <p class="standardSectionText">Want us to add a location? Why not get in touch to add your perfect dive site or location?</p>
                        <a id="getInTouch" href="#" class="btn btn-light-red getInTouch">Get in touch</a>
                    </div>
                </div>
            ';
      
        }
        echo '</section>';
    }

