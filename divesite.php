<?php 

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once "database/DiveSite.php";
require_once "utils/functions.php";
    $preview = isset($_POST['preview']) && $_POST['preview'] == true ? true : false;

    if(!$preview) {
        echo 'this is not a preview';
        $id = $_GET['id'];
        $diveSite = DiveSite::findById($id);
        $rating = $diveSite->getValue('rating');
        $diverLevel = $diveSite->getValue('diverLevel');
        $depth = $diveSite->getValue('depth');
        $visibility = $diveSite->getValue('visibility');
        $locationText = $diveSite->getValue('locationText');
        $isDiveSite = $diveSite->getValue('isDiveSite') == 1 ? true : false;
        $imagePaths = json_decode( $diveSite->getValue('locationImagePaths'));
        $locationHeader = $diveSite->getValue('locationTextTitle');
        $ratingComment = $diveSite->getValue('ratingComment');
        $current = $diveSite->getValue('current');
        $currentLevels = ['Weak', 'Normal', 'Average', 'Strong', 'Extra Strong'];
        $depthPercentage = (intval($depth) / 60 * 100) . "%";
        $visibilityPercentage = (intval($visibility) / 60 * 100) . "%";
        $visibilityPercentage = (int)($visibility / 40 * 100) . '%';
    } else {
      //  echo 'this is a preview';
    }

    $lightOnPaths = [];
    $lightOffPaths = [];

    for ($index = 1; $index <= 5; $index++) {
        $lightOnPaths[] = 'img/Level ' . $index . ' On.png';
        $lightOffPaths[] = 'img/Level ' . $index . ' Off.png';
    }
    

    if (!$preview) {
        require_once 'partials/header.php';
    } else {
        $data = array(
            'heading' => $_POST['heading'],
            'subheading' => $_POST['subheading'],
            'thumbnailPath' => $_POST['thumbnail'],
            'locationImagePaths' => $_POST['locationImagePaths'],
            'rating' => $_POST['rating'],
            'locationText' => $_POST['locationText'],
            'diverLevel' => $_POST['diverLevel'],
            'depth' => $_POST['depth'],
            'visibility' => $_POST['visibility'],
            'current' => $_POST['current'],
        );
        $diveSite = new DiveSite($data);
        $diveSite->setValue('isDiveSite', $_POST['isDiveSite']);
        $rating = $diveSite->getValue('rating');
        $isDiveSite = $diveSite->getValue('isDiveSite');
        $imagePaths = json_decode( $diveSite->getValue('locationImagePaths'));
        $locationText = $diveSite->getValue('locationText');
        $diverLevel = $diveSite->getValue('diverLevel');
        $depth = $diveSite->getValue('depth');
        $visibility = $diveSite->getValue('visibility');
        $current = $diveSite->getValue('current');
    }


    if ($isDiveSite === true) {
        $background = 'linear-gradient(112.5deg, rgba(16, 70, 48, 0.7) 85%, transparent 15%)';
    } else {
        $background = 'linear-gradient(112.5deg, rgba(23, 18, 62, 0.7) 85%, transparent 15%)';
    }
?>

    <?php echo Utils::getStandardDivesiteHeader($diveSite, $background, true); ?>


    <div class="location sliderStoppingPoint">
        <?php if ($isDiveSite) { ?>
            <h1 class="locationTextHeader largeHeader">The location</h1>
        <?php } else { ?>
            <h1 class="locationTextHeader largeHeader"><?php echo $locationHeader; ?></h1>
        <?php } ?>
        <p class="standardSectionText locationText"><?php echo $locationText ?></p>
     

        <?php if ($isDiveSite) { ?>

        <?php
            $imagePaths[0] = $preview ? $imagePaths[0] : 'img/divesites/location_images/' . $imagePaths[0];
            $imagePaths[1] = $preview ? $imagePaths[1] : 'img/divesites/location_images/' . $imagePaths[1];
            $imagePaths[2] = $preview ? $imagePaths[2] : 'img/divesites/location_images/' . $imagePaths[2];
            $imagePaths[3] = $preview ? $imagePaths[3] : 'img/divesites/location_images/' . $imagePaths[3];
        ?>
        

        <div class="mainImageContainer">
            <img style="width: 100%;" class="mainImage" src=" <?php echo $imagePaths[0] ?>">
        </div>
        <div class="smallImages">                         
            <div class="smallImage"><div class="hiddenPlus"><img class="hiddenPlusImage" src="img/plus.png"></div><img src="<?php echo $imagePaths[1]; ?>"><div class="smallImagePadding"></div></div>
            <div class="smallImage"><div class="hiddenPlus"><img class="hiddenPlusImage" src="img/plus.png"></div><img src="<?php echo $imagePaths[2]; ?>"><div class="smallImagePadding"></div></div>
            <div class="smallImage"><div class="hiddenPlus"><img class="hiddenPlusImage" src="img/plus.png"></div><img src="<?php echo $imagePaths[3]; ?>"><div class="smallImagePadding"></div></div>
        </div>
        <?php } ?>
    
    </div>

    <?php if ($isDiveSite) { 
        Utils::printDiverStatisticsHTML($diverLevel, $depth, $visibility, $current);
        
        ?>
    
    
    
        
    
    <?php } ?>
        <div class="virtualTourContainer sliderStoppingPoint">
            <div class="streetViewOverlay"></div>
                <?php echo $diveSite->getValue('virtualTourUrl'); ?>
        </div>
    <?php if (!$isDiveSite) { ?>
        
    
        <div class="ourRating">
            <h1 class="largeHeader">Our Rating</h1>

            <div class="stars">
                <?php
                    for ($index = 0; $index < 5; $index++) {
                        if ($index < $rating) {
                            if ($rating - $index < 1) {
                                echo '<img class="star" style="width: 28px;" src="img/' . 'halfStar.png' . '">';    
                            } else {
                                echo '<img class="star" style="width: 28px;" src="img/' . 'fullStar.png' . '">';
                            }
                        } else {
                            echo '<img class="star" style="width: 28px;" src="img/' . 'emptyStar.png' . '">';
                        }
                    }
                ?>
            </div>
            <div class="ratingComment">
                <p class="standardSectionText"><?php echo $ratingComment; ?></p>
                <a href="#" class="btn btn-green">View</a><a href="#" class="btn btn-light-red">Get In Touch</a>
            </div>
              
        </div>
    <?php } ?>
    
    

    <?php echo Utils::getIslands(); ?>


    <?php if ($isDiveSite) { ?>
    
        <section class="databaseAndExplore">
            <section class="database">
                <section>
                    <h2 class="mediumHeader">Check out our other destinations</h2>
                    <p class="standardSectionText">
                        Dive Thailand is an organisation fueled by professional divers based within the
                        U.K. We’re commited in aiding divers looking at Thailand and its surrounding
                        islands as thier next diving destination. Helping divers know where to go where to
                        dive and where to stay.
                    </p>
                    <a class="btn btn-green" href="#">Search</a>
                </section>
            </section>
            <section class="explore">
                <section>
                    <h2 class="mediumHeader">Like this dive site?</h2>
                    <p class="standardSectionText">
                        Dive Thailand is an organisation fueled by professional divers based within the
                        U.K. We’re commited in aiding divers looking at Thailand and its surrounding
                        islands as thier next diving destination. Helping divers know where to go where to
                        dive and where to stay.
                    </p>
                    <a class="btn btn-light-red" href="#">Enquire</a>
                </section>
            </section>
        </section>

        <section class="subscribeArea">
                <h2 class="mediumHeader">Subscribe for new locations and diving news</h2>
                <p class="standardSectionText">
                    Dive Thailand is an organisation fueled by professional divers based within the U.K. We’re commited in aiding divers
                    looking at Thailand and its surrounding islands as thier next diving destination.
                </p>
                <div class="subscribeControls">
                    <input type="text" placeholder="Email"><a class="btn btn-light-green subscribe" href="#">Subscribe</a>
                </div>
            </section>
                
        
    <?php } ?>

<script>
    $('.smallImage img').click(function(evt) {
        evt.preventDefault();
        var thisSrc = $(this).attr('src');
        var mainImgSrc = $('.mainImage').attr('src');
        $('.mainImage').attr('src', thisSrc);
        $(this).attr('src', mainImgSrc);
    });


    $(document).ready(function() {
        console.log('calling slider code')
        $('body').screenSlider(['white', 'black', 'white', 'black', 'white']);

        $('.streetViewOverlay').click(function(evt) {
            $(this).addClass('streetViewOverlayClicked');
        });
      
        $('.virtualTourContainer').mouseleave(function(evt) {
            $('.streetViewOverlay').removeClass('streetViewOverlayClicked');
        });
    });

    </script>

<?php require_once 'partials/footer.php' ?>